# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

##############################################
##############################################

## Create the db schema
CREATE SCHEMA `apirezzemolofresco_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

## Setup the db, vendor and initial setup: from terminal type
## Requires composer and php7>
composer install
php artisan migrate
composer dump-autoload // everytime a class is added
php artisan db:seed

## generate the encryption key
php artisan key:generate