<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <img style="max-width: 100%; width: 160px;" src="https://prezzemolofresco.com/assets/logoXS.png" />
                <br />
                <br />
                <h3 style="font-family: Tahoma; color: #8f2785;" class="panel-heading">CONFIRMED</h3>
                <p style="font-family: Tahoma" class="panel-body">
                    Your Email is successfully verified.
                    <br />
                    <a style="font-family: Tahoma; font-weight: bold; color: #8f2785;" href="https://prezzemolofresco.com">LOGIN</a>
                </p>
                <br />
                <img style="max-width: 40%; width: 40px;" src="https://prezzemolofresco.com/assets/pf.png" />
            </div>
        </div>
    </div>
</div>
