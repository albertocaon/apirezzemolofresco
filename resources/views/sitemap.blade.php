<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://prezzemolofresco.com/dashboard</loc>
    </url>
    <url>
        <loc>https://prezzemolofresco.com/authors</loc>
    </url>
    <url>
        <loc>https://prezzemolofresco.com/hashtags</loc>
    </url>
    <url>
        <loc>https://prezzemolofresco.com/info</loc>
    </url>
    @foreach($posts as $post)
        <url>
            <loc>https://prezzemolofresco.com/detail/{{ $post->id }}/{{ $post->prettyTitle }}</loc>
        </url>
        <url>
            <loc>https://prezzemolofresco.com/detail/{{ $post->id }}</loc>
        </url>
    @endforeach

    @foreach($authors as $author) 
        <url>
            <loc>https://prezzemolofresco.com/author/{{ $author->username }}</loc>
        </url>
    @endforeach

    @foreach($hashtags as $hashtag) 
        <url>
            <loc>https://prezzemolofresco.com/hashtag/{{ $hashtag }}</loc>
        </url>
    @endforeach

    <!-- @for ($i = 1; $i < 50; $i++)
        <url>
            <loc>https://prezzemolofresco.com/top/{{ $i }}</loc>
        </url>
    @endfor -->
</urlset>