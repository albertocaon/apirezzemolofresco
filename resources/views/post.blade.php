{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title') !!}
		</li>
		<li>
			{!! Form::label('idUser', 'IdUser:') !!}
			{!! Form::text('idUser') !!}
		</li>
		<li>
			{!! Form::label('image', 'Image:') !!}
			{!! Form::text('image') !!}
		</li>
		<li>
			{!! Form::label('description', 'Description:') !!}
			{!! Form::text('description') !!}
		</li>
		<li>
			{!! Form::label('recipe', 'Recipe:') !!}
			{!! Form::text('recipe') !!}
		</li>
		<li>
			{!! Form::label('vegan', 'Vegan:') !!}
			{!! Form::text('vegan') !!}
		</li>
		<li>
			{!! Form::label('veggie', 'Veggie:') !!}
			{!! Form::text('veggie') !!}
		</li>
		<li>
			{!! Form::label('fish', 'Fish:') !!}
			{!! Form::text('fish') !!}
		</li>
		<li>
			{!! Form::label('meat', 'Meat:') !!}
			{!! Form::text('meat') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}