<style>
* {
	font-family: Tahoma;
	font-size: 14px;
}

.container {
	padding: 15px 20px;
}

h1 {
	font-size: 16px;
	color: #8f2785;
}

a {
	color: #8f2785;
	font-size: 16px;
}

#header {
	max-width: 100%;
	width: 160px;
}

#signature {
	max-width: 40%;
	width: 40px;
}
</style>
<div class="container">

	<img id="header" src="https://prezzemolofresco.com/assets/logoXS.png" />
	<h1>You're one step to go!</h1>

	<p>Click the following link or paste it in the browser to verify your email<br />
	<a href="{{url('/api/verifyemail/'.$email_token)}}">{{url('/api/verifyemail/'.$email_token)}}</a></p>
	<br />
	<br />
	<img id="signature" src="https://prezzemolofresco.com/assets/pf.png" />
</div>