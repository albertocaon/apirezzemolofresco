{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('idPost', 'IdPost:') !!}
			{!! Form::text('idPost') !!}
		</li>
		<li>
			{!! Form::label('idUser', 'IdUser:') !!}
			{!! Form::text('idUser') !!}
		</li>
		<li>
			{!! Form::label('rating', 'Rating:') !!}
			{!! Form::text('rating') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}