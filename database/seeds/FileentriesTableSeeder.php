<?php

use App\Fileentry;
use Illuminate\Database\Seeder;

class FileentriesTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// DefaultPostImage
		Fileentry::create(
			array(
				'id' => 1,
				'filename' => 'defaultParsleyPostImage.jpg',
				'mime' => 'image/jpeg',
				'original_filename' => 'defaultParsleyPostImage.jpg',
			)
		);

		// DefaultUserImage
		Fileentry::create(
			array(
				'id' => 2,
				'filename' => 'defaultParsleyUserImage.png',
				'mime' => 'image/png',
				'original_filename' => 'defaultParsleyUserImage.png',
			)
		);
	}
}
