<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	public function run() {
		Model::unguard();

		$this->call('UsersTableSeeder');
		$this->call('FileentriesTableSeeder');
		$this->command->info('User table seeded!');
	}
}