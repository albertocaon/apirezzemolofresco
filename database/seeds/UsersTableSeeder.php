<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

	public function run() {
		//DB::table('user')->delete();

		// Let's make sure everyone has the same password and
		// let's hash it before the loop, or else our seeder
		// will be too slow.
		$password = Hash::make('freshparsley');

		// firstDefaultUser
		User::create(array(
			'username' => 'masterchef',
			'email' => 'alberto@letscode.es',
			'password' => $password,
			'role' => 'Admin',
		));

		// bebo
		$password = Hash::make('4r4MaEsCo');
		User::create(array(
			'username' => 'beboyasu',
			'email' => 'albertocaon@icloud.com',
			'password' => $password,
			'role' => 'User',
		));

		// edo
		$password = Hash::make('2fradeighinda');
		User::create(array(
			'username' => 'edo',
			'email' => 'edoardo.caon@gmail.com',
			'password' => $password,
			'role' => 'User',
		));
	}
}