<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropDefaultLangColFromPost extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('post', function (Blueprint $table) {
			$table->dropColumn('defaultLanguage');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('post', function (Blueprint $table) {
			$table->enum('defaultLanguage', ['catalan', 'english', 'italian', 'spanish']);
		});
	}
}
