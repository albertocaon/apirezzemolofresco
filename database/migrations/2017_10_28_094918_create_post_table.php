<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration {

	public function up() {
		Schema::create('post', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('idUser');
			$table->string('image')->default(1);
			$table->boolean('vegan');
			$table->boolean('veggie')->nullable();
			$table->boolean('fish');
			$table->boolean('meat');
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('post');
	}
}