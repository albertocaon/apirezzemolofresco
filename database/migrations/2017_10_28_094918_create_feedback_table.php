<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeedbackTable extends Migration {

	public function up() {
		Schema::create('feedback', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('idPost');
			$table->integer('idUser');
			$table->enum('rating', array('1', '2', '3', '4', '5'))->nullable();
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('feedback');
	}
}