<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OnlyOneLanguagePosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('extra');
        Schema::table('post', function (Blueprint $table) {
            $table->string('title', 255);
            $table->string('description', 255)->nullable();
            $table->longText('ingredients')->nullable();
            $table->longText('recipe')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('extra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idPost');
            $table->string('title', 255);
            $table->enum('language', array('catalan', 'english', 'italian', 'spanish'));
            $table->string('description', 255)->nullable();
            $table->longText('ingredients');
            $table->longText('recipe');
            $table->timestamps();
        });
        Schema::table('post', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('ingredients');
            $table->dropColumn('recipe');
        });
    }
}
