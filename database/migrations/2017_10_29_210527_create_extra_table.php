<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExtraTable extends Migration {

	public function up() {
		Schema::create('extra', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('idPost');
			$table->string('title', 255);
			$table->enum('language', array('catalan', 'english', 'italian', 'spanish'));
			$table->string('description', 255)->nullable();
			$table->longText('ingredients');
			$table->longText('recipe');
			$table->timestamps();
		});
	}

	public function down() {
		Schema::drop('extra');
	}
}