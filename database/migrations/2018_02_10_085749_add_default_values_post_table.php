<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValuesPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post', function (Blueprint $table) {
            $table->string('vegan')->default(0)->change();
            $table->string('veggie')->default(0)->nullable(false)->change();
            $table->string('fish')->default(0)->change();
            $table->string('meat')->default(0)->change();
            $table->string('title')->default('The Title')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post', function (Blueprint $table) {
        });
    }
}
