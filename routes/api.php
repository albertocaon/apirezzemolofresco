<?php

use App\Post;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/* User */
Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

/*Route::get('allthumbnails', [
'as' => 'getentry', 'uses' => 'FileEntryController@allThumbnails']);*/

Route::get('sitemap.xml', 'PostController@sitemap');

Route::get('phpinfo', 'Auth\LoginController@phpinfo');

Route::get('ping', 'Auth\LoginController@ping');

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::get('logout', 'Auth\LoginController@logout');
Route::post('password/email', 'Auth\ForgotPasswordController@getResetToken');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('userpasswordreset', 'Auth\ForgotPasswordController@sendResetLink');
Route::post('getuserbyremembertoken', 'Auth\ResetPasswordController@getUserByRememberToken');

Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

Route::post('welcome', 'PostController@index');
Route::get('thumbnail/{filename}', [
		'as' => 'getentry', 'uses' => 'FileEntryController@getThumbnail']);

/* PUBLIC ROUTES */ 
/* Author */
Route::get('author/{userId}', 'UserController@show');
// Route::get('authors', 'UserController@index');
Route::post('authors', 'UserController@getAuthors');
// Route::get('searchauthors/{term}', 'UserController@searchAuthor');
Route::post('incrementauthorviews', 'UserController@incrementViews');

/* Post */
Route::get('getposts/{pageNumber}/{pageSize}/{sortOrder}/{filters}/{foodType}', 'PostController@getPosts');
Route::get('getpostscount/{pageNumber}/{pageSize}/{sortOrder}/{filters}/{foodType}', 'PostController@getPostsCount');
Route::post('pageposts', 'PostController@index');
Route::post('mostviewed', 'PostController@getMostViewed');
Route::post('search', 'PostController@search');
Route::post('totalposts', 'PostController@total');
Route::get('post/{postId}', 'PostController@show');
Route::post('incrementpostviews', 'PostController@incrementViews');
Route::get('hashtags', 'PostController@getHashtags');
Route::post('matchinghashtags', 'PostController@getMatchingHashtags');


/* Feedback */
// Route::get('feedbacks', 'FeedbackController@index');
Route::get('feedback/{userId}/{postId}', 'FeedbackController@userRate');
Route::get('feedbacks/{idPost}', 'FeedbackController@postRatings');

/* Files */
Route::get('file', 'FileEntryController@index');
Route::get('file/{filename}', [
	'as' => 'getentry', 'uses' => 'FileEntryController@get']);
/* EO PUBLIC ROUTES */


Route::group(['middleware' => 'auth:api'], function () {
	/* Author */
	// Route::get('author/{userId}', 'UserController@show');
	// Route::get('authors', 'UserController@index');
	// Route::get('authors', 'UserController@getAuthors');
	Route::post('updateprofile', 'UserController@update');

	/* Post */
	// Route::post('pageposts', 'PostController@index');
	// Route::post('search', 'PostController@search');
	// Route::post('totalposts', 'PostController@total');
	// Route::get('post/{postId}', 'PostController@show');
	Route::post('posts', 'PostController@store');
	Route::post('updatepost', 'PostController@update');
	Route::post('deletepost', 'PostController@delete');
	Route::get('gettop/{howMany}', 'PostController@getTop');

	/* Extra */
	Route::post('deleteextra', 'ExtraController@delete');

	/* Feedback */
	// Route::get('feedbacks', 'FeedbackController@index');
	// Route::get('feedback/{userId}/{postId}', 'FeedbackController@userRate');
	// Route::get('feedbacks/{idPost}', 'FeedbackController@postRatings');
	Route::post('storeRate', 'FeedbackController@storeUserRate');
	Route::post('feedbacks', 'FeedbackController@store');
	// Route::post('updatefeedback', 'FeedbackController@update');
	// Route::post('deletefeedback', 'FeedbackController@delete');

	/* Files */
	// Route::get('file', 'FileEntryController@index');
	// Route::get('file/{filename}', [
		// 'as' => 'getentry', 'uses' => 'FileEntryController@get']);
	
	Route::post('file', [
		'as' => 'addentry', 'uses' => 'FileEntryController@add']);
});