<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MailResetPasswordToken;


class User extends Authenticatable {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'username', 'email', 'password', 'profileImage', 'role', 'email_token', 'mobile', 'showMobile', 'mail', 'showMail', 'siteUrl', 'showSiteUrl'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function generateToken() {
		$this->api_token = str_random(60);
		$this->save();

		return $this->api_token;
	}

	/**
	 * Send a password reset email to the user
	 */
	public function sendPasswordResetNotification($token)
	{
		$this->remember_token = $token;
		$this->save();
	    $this->notify(new MailResetPasswordToken($token));
	}

}
