<?php
namespace App\Http\Controllers;

use App\Fileentry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use Request;

class FileEntryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$entries = Fileentry::all();

		return view('fileentries.index', compact('entries'));
	}

	public function add(Request $request) {
		$file = Request::file('file');
		$extension = $file->getClientOriginalExtension();
		$filename = $file->getFilename();
		Storage::disk('local')->put($filename . '.' . $extension, File::get($file));
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename() . '.' . $extension;

		$entry->save();

		$image = Image::make($file);

		$image->resize(270, 270);

		$thumbnail_image_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $file->getClientOriginalExtension();

		$image->save(public_path('images/' . $thumbnail_image_name));

		$saved_image_uri = $image->dirname . '/' . $image->basename;

		//Now use laravel filesystem.
		$uploaded_thumbnail_image = Storage::disk('local')->put('thumbnail_' . $filename . '.' . $extension, File::get($saved_image_uri));

		//Now delete temporary intervention image as we have moved it to Storage folder with Laravel filesystem.
		$image->destroy();
		unlink($saved_image_uri);

		return json_encode($entry);
	}

	/*public function allThumbnails() {
		$fileentries = Fileentry::all();
		$files = array();

		foreach ($fileentries as $k => $fileentry) {
			if (Storage::disk('local')->exists($fileentry->filename)) {
				$file = Storage::disk('local')->get($fileentry->filename);
				$filename = $fileentry->filename;
				$mime = $fileentry->mime;

				$path_info = pathinfo($fileentry->filename);

				$extension = $path_info['extension'];

				$image = Image::make($file);

				$image->resize(270, 270);

				$thumbnail_image_name = pathinfo($filename, PATHINFO_FILENAME) . '.' . $extension;

				$image->save(public_path('images/' . $thumbnail_image_name));

				$saved_image_uri = $image->dirname . '/' . $image->basename;

				//Now use laravel filesystem.
				$uploaded_thumbnail_image = Storage::disk('local')->put('thumbnail_' . $filename, File::get($saved_image_uri));

				//Now delete temporary intervention image as we have moved it to Storage folder with Laravel filesystem.
				$image->destroy();
				unlink($saved_image_uri);

				echo $k . ' compressed!<br />';

			} else {
				$fileentry->delete();
			}
		}

		echo 'Done!';
	}*/

	public function get($filename) {

		$entry = Fileentry::where('filename', '=', $filename)->firstOrFail();
		$file = Storage::disk('local')->get($entry->filename);

		return (new Response($file, 200))
			->header('Content-Type', $entry->mime);
	}

	public function getThumbnail($filename) {

		$entry = Fileentry::where('filename', '=', $filename)->firstOrFail();
		$file = Storage::disk('local')->get('thumbnail_' . $entry->filename);

		return (new Response($file, 200))
			->header('Content-Type', $entry->mime);
	}
}