<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Fileentry;
use App\User;
use App\Post;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 * Returns a list of essentials of all users
	 *
	 * @return Response
	 */
	public function index() {
		$author = User::join('fileentries', 'fileentries.id', '=', 'users.profileImage')
			->leftJoin('post', 'post.idUser', '=', 'users.id')
			->leftJoin('feedback', 'feedback.idPost', '=', 'post.id')
			->select(array('users.id as id', 'users.views as views', 'users.username as username', 'fileentries.filename as profileImageUrl', DB::raw('AVG(feedback.rating) as rating'), DB::raw('COUNT(post.id) as postCount')))
			->groupBy('users.id')
			->orderBy('rating', 'desc')
			->get();

		return response()->json($author, 200);
	}

	/**
	 * Display a listing of the resource.
	 * Returns a list of essentials of all users or filtered by param
	 *
	 * @param string $filter
	 * @return Response
	 */
	public function getAuthors(Request $request) {
		$filter = $request->filter ? $request->filter : '';
		$sortOrder = $request->sortOrder;
		$pageNumber = (int) $request->pageNumber;
		$pageSize = (int) $request->pageSize;
		$skip = $pageNumber * $pageSize;

		// var_dump($filter);
		if ($filter == '') {
			$authors = User::leftJoin('fileentries', 'fileentries.id', '=', 'users.profileImage');
		} else {
			$authors = User::where('users.username', 'like', '%'.$filter.'%')
				->orWhere('users.email', 'like', '%'.$filter.'%')
				->orWhere('users.siteUrl', 'like', '%'.$filter.'%')
				->leftJoin('fileentries', 'fileentries.id', '=', 'users.profileImage');
		}
		$authors = $authors->leftJoin('post', 'post.idUser', '=', 'users.id')
			->select(array(
				'users.id as id',
				DB::raw('(SELECT SUM(post.views) FROM apirezzemolofresco_db.post post 
					LEFT JOIN apirezzemolofresco_db.users users3 ON users3.id = post.idUser 
					WHERE users3.id = users.id) as views'),
				'users.username as username', 
				'fileentries.filename as profileImageUrl', 
				DB::raw('(SELECT AVG(feedback.rating) FROM apirezzemolofresco_db.users users2 
					LEFT JOIN apirezzemolofresco_db.post post ON users2.id = post.idUser 
					LEFT JOIN apirezzemolofresco_db.feedback feedback ON feedback.idPost = post.id
					WHERE users2.id = users.id) as rating'), 
				DB::raw('COUNT(post.id) as postCount'), 
				'users.mobile as mobile', 
				'users.email as email', 
				'users.siteUrl as siteUrl', 
				'users.showMail as showMail', 
				'users.showMobile as showMobile', 
				'users.showSiteUrl as showSiteUrl'
			))
			->take($pageSize)
			->skip($skip)
		    ->groupBy('users.id')
			->orderBy('views', 'desc')
			->get();

		return response()->json($authors, 200);
	}
		/**
	 * Increment the views counter of an author list of posts
	 *
	 * @return Response
	 */
	public function incrementViews(Request $request) {
		$user = User::find($request->userId);
		$user->views++;

		$user->save();

		return response()->json(true, 200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id) {
		$author = User::where('id', $id)->first();
		$author->password = '';
		$author->remember_token = '';
		$author->api_token = '';
		$author->profileImageUrl = (Fileentry::where('id', $author->profileImage)->first()->filename) ? Fileentry::where('id', $author->profileImage)->first()->filename : 'defaultParsleyUserImage.png';
		$author->rating = Feedback::join('post', 'post.id', '=', 'feedback.idPost')->where('post.idUser', $id)->selectRaw('AVG(feedback.rating) as rating')->first()->rating;
		$author->postCount = Post::where('idUser', '=', $id)->count();
		return response()->json($author, 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string $term
	 * @return Response
	 */
	public function searchAuthor($term) {
		$term = '%'.$term.'%';
		$authors = User::where('username', 'like', $term)
			->orWhere('email', 'like', $term)
			->orWhere('siteUrl', 'like', $term)
			->get();
			foreach ($authors as $k => $author) {
				$authors[$k]->password = '';
				$authors[$k]->remember_token = '';
				$authors[$k]->api_token = '';
				$authors[$k]->profileImageUrl = (Fileentry::where('id', $authors[$k]->profileImage)->first()->filename) ? Fileentry::where('id', $authors[$k]->profileImage)->first()->filename : 'defaultParsleyUserImage.png';
				$authors[$k]->rating = Feedback::join('post', 'post.id', '=', 'feedback.idPost')->where('post.idUser', $id)->selectRaw('AVG(feedback.rating) as rating')->first()->rating;
				$authors[$k]->postCount = Post::where('idUser', '=', $id)->count();
			}

		return response()->json($authors, 200);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  User $user
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request) {
		$user = User::find($request->id);
		$user->fill($request->all())->save();

		return response()->json($user, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {

	}

}

?>