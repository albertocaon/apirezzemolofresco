<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Post;
use App\User;
use App\Fileentry;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller {

	/**
	 * Display a listing of the resource.
	 * it also adds the correspondents extra
	 *
	 * @return Response
	 */
	public function index(Request $request) {
		$sortOrder = $request->sortOrder;
		$pageNumber = (int) $request->pageNumber;
		$pageSize = (int) $request->pageSize;
		$skip = $pageNumber * $pageSize;

		$posts = Post::leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->leftJoin('feedback', 'feedback.idPost', '=', 'post.id')
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.created_at', 'post.views',
				DB::raw('AVG(rating) as rating'),
				DB::raw('fileentries.filename as imageUrl'),
			))
			->take($pageSize)
			->skip($skip)
			->groupBy('post.id')
			->orderBy('post.created_at', 'DESC')
			->get();

		return response()->json($posts, 200);
	}

	public function getMostViewed(Request $request) {
		$sortOrder = $request->sortOrder;
		$pageNumber = (int) $request->pageNumber;
		$pageSize = (int) $request->pageSize;
		$skip = $pageNumber * $pageSize;

		$posts = Post::leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->leftJoin('feedback', 'feedback.idPost', '=', 'post.id')
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.created_at', 'post.views',
				DB::raw('AVG(rating) as rating'),
				DB::raw('fileentries.filename as imageUrl'),
			))
			->take($pageSize)
			->skip($skip)
			->groupBy('post.id')
			->orderBy('post.views', 'DESC')
			->get();

		return response()->json($posts, 200);
	}

	public function getTop(Request $request) {
		$howMany = (int) $request->howMany;

		$posts = Post::leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.created_at', 'post.views',
				DB::raw('fileentries.filename as imageUrl'),
			))
			->take($howMany)
			->groupBy('post.id')
			->orderBy('post.views', 'DESC')
			->get();

		return response()->json($posts, 200);
	}

	public function search(Request $request) {
		$columns = array('post.title', 'post.recipe', 'post.description', 'post.ingredients');
		$filters = $request->filters;
		$foodType = $request->foodType;
		$sortOrder = $request->sortOrder;
		$pageNumber = (int) $request->pageNumber;
		$pageSize = (int) $request->pageSize;
		$skip = $pageNumber * $pageSize;

		$posts = Post::from('post as post')
			->leftJoin('users', 'users.id', '=', 'post.idUser')
			->leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->leftJoin('feedback', 'feedback.idPost', '=', 'post.id')
			->where(function ($query) use ($filters, $columns) {
				foreach ($columns as $col) {
					$query = $query->orWhere(function ($query) use ($filters, $col) {
						foreach ($filters as $filter) {
							$query = $query->where($col, 'LIKE', '%' . $filter['name'] . '%');
						}
					});
				}
			})
			->where(function ($query) use ($foodType) {
				foreach ($foodType as $type => $value) {
					if ($value) {
						$query = $query->where($type, '=', $value);
					}
				}
			})
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.views',
				DB::raw('AVG(rating) as rating'),
				DB::raw('fileentries.filename as imageUrl'),
			))
			->take($pageSize)
			->skip($skip)
			->groupBy('post.id')
			->orderBy('post.created_at', 'DESC')
			->get();

		return response()->json($posts, 200);
	}

	public function getPosts($pageNumber, $pageSize, $sortOrder, $filter, $foodType) {
		$columns = array('post.title', 'post.recipe', 'post.description', 'post.ingredients');
		$skip = $pageNumber * $pageSize;

		$filter = urldecode($filter);
		$foodType = json_decode($foodType);

		$posts = Post::from('post as post')
			->leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->orWhere(function ($query) use ($filter, $columns) {
				foreach ($columns as $col) {
					$query = $query->orWhere($col, 'LIKE', '%' . $filter . '%');
				}
			})
			->where(function ($query) use ($foodType) {
				if ($foodType && $foodType != '_') {
					foreach ($foodType as $type => $value) {
						if ($value) {
							$query = $query->where($type, '=', $value);
						}
					}
				}
			})
			->where('post.idUser', '=', 74) // only PF is visible
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.views',
				DB::raw('fileentries.filename as imageUrl'),
			))
			->take($pageSize)
			->skip($skip)
			->groupBy('post.id')
			->orderBy('post.created_at', 'DESC')
			->get();

		return response()->json($posts, 200);
	}

		public function getPostsCount($pageNumber, $pageSize, $sortOrder, $filter, $foodType) {
		$columns = array('post.title', 'post.recipe', 'post.description', 'post.ingredients');

		$filter = urldecode($filter);
		$foodType = json_decode($foodType);

		$count = Post::from('post as post')
			->leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->orWhere(function ($query) use ($filter, $columns) {
				foreach ($columns as $col) {
					$query = $query->orWhere($col, 'LIKE', '%' . $filter . '%');
				}
			})
			->where(function ($query) use ($foodType) {
				if ($foodType && $foodType != '_') {
					foreach ($foodType as $type => $value) {
						if ($value) {
							$query = $query->where($type, '=', $value);
						}
					}
				}
			})
			->where('post.idUser', '=', 74) // only PF is visible
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.views',
				DB::raw('fileentries.filename as imageUrl'),
			))
			->groupBy('post.id')
			->get()	
			->count();

		return response()->json($count, 200);
	}

	/**
	 * Give the total count of posts
	 *
	 * @return Response
	 */
	public function total(Request $request) {
		$columns = array('post.title', 'post.recipe', 'post.description', 'post.ingredients');
		$filters = (count($request->filters) > 0) ? $request->filters : array();
		$sortOrder = $request->sortOrder;
		$pageNumber = (int) $request->pageNumber;
		$pageSize = (int) $request->pageSize;
		$skip = $pageNumber * $pageSize;

		$total = Post::from('post as post')
			->leftJoin('users', 'users.id', '=', 'post.idUser')
			->where(function ($query) use ($filters, $columns) {
				foreach ($columns as $col) {
					$query = $query->orWhere(function ($query) use ($filters, $col) {
						foreach ($filters as $filter) {
							$query = $query->where($col, 'LIKE', '%' . $filter['name'] . '%');
						}
					});
				}
			})
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.views',
				DB::raw('fileentries.filename as imageUrl'),
			))
			->count();

		return response()->json($total, 200);

	}

	/**
	 * Increment the views counter of a posts
	 *
	 * @return Response
	 */
	public function incrementViews(Request $request) {
		$post = Post::find($request->postId);
		$post->views++;

		return response()->json($post->save(), 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  Post $post
	 * @return Response
	 */
	public function show($id) {
		$post = Post::where('post.id', $id)->leftJoin('fileentries', 'fileentries.id', '=', 'post.image')
			->select(array('post.id', 'post.idUser', 'post.image', 'post.vegan', 'post.veggie', 'post.fish', 'post.meat', 'post.glutenFree', 'post.lactoseFree', 'post.title', 'post.description', 'post.ingredients', 'post.recipe', 'post.views', 
				DB::raw('fileentries.filename as imageUrl'),
				'post.created_at',
			))
			->where('post.id', '=', $id)
			->where('post.idUser', '=', 74)
			->first();

		return response()->json($post, 200);
	}

	function isHashtag($v) {
		return substr($v->name, 0, 1) === '#';
	}

	private function incrementHashtagCounter(array $hashtags, string $hashtagToIncrement) {
		if (count($hashtags) == 0) {
			$hashtags[] = array('name' => $hashtagToIncrement, 'quantity' => 1);
			return $hashtags;
		} else {
			foreach ($hashtags as $kincr => $incr) {
				if ($incr["name"] == $hashtagToIncrement) {
					$hashtags[$kincr]["quantity"] = $hashtags[$kincr]["quantity"] + 1;
					return $hashtags;
				} 

				if (count(array_filter(
					$hashtags, 
					function($h) use ($hashtagToIncrement) {
					return $h["name"] == $hashtagToIncrement;
				})) < 1) {
					$hashtags[] = array('name' => $hashtagToIncrement, 'quantity' => 1);
					return $hashtags;
				}

			}
			return $hashtags;
		}
	}

	/**
	 * gives all unique existent hashtags.
	 *
	 * @return Response
	 */
	public function getHashtags() {
		$posts = Post::select(array('post.ingredients'))->where('post.idUser', '=', 74)->where('post.vegan', '=', true)->groupBy('post.id')->get();
		$hashtags = array();
		foreach ($posts as $post) {
			$onlyHashtags = array_filter(json_decode($post->ingredients), array($this, 'isHashtag'));
			foreach($onlyHashtags as $ingr) {
				$hashtagToIncrement = substr($ingr->name, 1);
				if (substr($ingr->name, 0, 1) == '#') {
					$hashtags = $this->incrementHashtagCounter($hashtags, $hashtagToIncrement);
				} 
			}
		}
		$collection = collect($hashtags);
		$sorted = $collection->sortByDesc('quantity');
		return response()->json($sorted->values()->all(), 200);
	}

	/**
	 * gives all unique existent hashtags.
	 *
	 * @return Response
	 */
	public function getMatchingHashtags(Request $request) {
		$term = $request->term ? $request->term : '';
		$posts = Post::select(array('post.ingredients'))->where('post.idUser', '=', 74)->where('post.vegan', '=', true)->get();
		$hashtags = array();
		foreach ($posts as $post) {
			$onlyHashtags = array_filter(json_decode($post->ingredients), array($this, 'isHashtag'));
			foreach($onlyHashtags as $ingr) {
				$hashtagToIncrement = substr($ingr->name, 1);
				if ($term != '') {
					if (strtolower(substr($ingr->name, 1, strlen($term)-1)) == strtolower(substr($term, 1))) {
						$hashtags = $this->incrementHashtagCounter($hashtags, $hashtagToIncrement);
					}
				} else {
					$hashtags = $this->incrementHashtagCounter($hashtags, $hashtagToIncrement);
				}
			}
		}
		
		$collection = collect($hashtags);
		$sorted = $collection->sortByDesc('quantity');
		return response()->json($sorted->values()->all(), 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$idUser = Auth::guard('api')->user()['id'];
		$post = Post::create($request->all());

		return response()->json($post, 201);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Post $post
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request) {
		$post = Post::find($request->id);
		$post->fill($request->all())->save();

		return response()->json($post, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Post $post
	 * @param Request $request
	 * @return Response
	 */
	public function delete(Request $request) {
		$post = Post::find($request->id);
		$ratings = Feedback::where('idPost', $post->id)->delete();
		$post->delete();
		$this->cleanUnusedFilesAndReferencesOfPhotosFromStorage();

		return response()->json(null, 204);
	}

    private function cleanUnusedFilesAndReferencesOfPhotosFromStorage()
    {
        $allFiles = Storage::allFiles();
        $usedPostEntries = Post::select('image')->where('idUser', '=', 74)->get();
        $defaultPostImage = new Post;
        $defaultPostImage->image = "1";
        $usedPostEntries->push($defaultPostImage);
        $usedUserEntries = User::select('profileImage')->get();
		$defaultUserImage = new Post;
		$defaultUserImage->image = "2";        
        $usedUserEntries->push($defaultUserImage);
        $usedEntries = $usedPostEntries->concat($usedUserEntries);
        $usedPhotos = Fileentry::whereIn('id', $usedEntries)->select('filename')->get();
        $usedFileNames = array();
        foreach ($usedPhotos as $usedPhoto) {
            array_push($usedFileNames, $usedPhoto->filename);
            array_push($usedFileNames, 'thumbnail_' . $usedPhoto->filename);
        }
        Fileentry::whereNotIn('id', $usedEntries)->delete();
        Storage::delete(array_diff($allFiles, $usedFileNames));
    }


	public function sitemap()
	{
	    $posts = Post::all();
	    $authors = User::join('post', 'post.idUser', '=', 'users.id')->where('post.idUser', '=', 74)->groupBy('users.id')->get();
	    $hashtags = array();
		foreach ($posts as $post) {
			$post->prettyTitle = preg_replace('/[^a-zA-Z0-9\']/', '_', $post->title);
			// $post->prettyTitle = 'da_taitol';
			foreach(json_decode($post->ingredients) as $ingr) {
				if (substr($ingr->name, 0, 1) == '#' && !in_array(substr($ingr->name, 1), $hashtags)) {
					$hashtags[] = substr($ingr->name, 1);
				}
			}
		}
	    return response()->view('sitemap', compact('posts', 'authors', 'hashtags'))->header('Content-Type', 'text/xml');
	}

}

?>