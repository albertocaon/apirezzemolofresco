<?php

namespace App\Http\Controllers;

use App\Feedback;
use DB;
use Illuminate\Http\Request;

class FeedbackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		return Feedback::all();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  Feedback $feedback
	 * @return Response
	 */
	public function show(Feedback $feedback) {
		return $feedback;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  idPost $idPost
	 * @return Response
	 */
	public function postRatings($idPost) {
		$totalVotes = Feedback::where('idPost', $idPost)
			->count();
		$feedbacks = Feedback::from('feedback as feedback')
			->where('idPost', $idPost)
			->select(array(DB::raw('COUNT(*) as votes'), DB::raw('floor(round(COUNT(rating) *100 / ' . $totalVotes . ')) as percentage'), 'rating'))
			->orderBy('rating', 'DESC')
			->groupBy('rating')
			->get();

		return response()->json(array('ratings' => $feedbacks, 'totalVotes' => $totalVotes), 200);
	}

	/**
	 * Display the specified resource of a particular user.
	 *
	 * @param  userId $userId
	 * @param  postId $postId
	 * @return Response
	 */
	public function userRate($idUser, $idPost) {
		$rate = Feedback::from('feedback as feedback')
			->join('users', 'users.id', '=', 'feedback.idUser')
			->where('idPost', $idPost)->where('idUser', $idUser)->first();
		if (!is_null($rate)) {
			$rating = $rate->rating;
		} else {
			$rating = 0;
		}
		return response()->json($rating, 200);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeUserRate(Request $request) {
		$rateCount = Feedback::where('idPost', $request->idPost)->where('idUser', $request->idUser)->count();
		if ($rateCount < 1) {
			// if no rate is found let's create a new one
			$feedback = Feedback::create($request->all());
			$result = $feedback->rating;
		} else {
			// if a rating already exists let's overwrite it
			$feedback = Feedback::where('idPost', $request->idPost)->where('idUser', $request->idUser)->first();
			$feedback->update($request->all());
			$result = $feedback->rating;
		}

		return response()->json($result, 201);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Feedback $feedback
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, Feedback $feedback) {
		$feedback->update($request->all());

		return response()->json($feedback, 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Feedback $feedback
	 * @param Request $request
	 * @return Response
	 */
	public function delete(Request $request, Feedback $feedback) {
		$feedback->delete();

		return response()->json(null, 204);
	}

}

?>