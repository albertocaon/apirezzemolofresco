<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	protected $table = 'post';
	protected $fillable = ['idUser', 'image', 'vegan', 'veggie', 'fish', 'meat', 'glutenFree', 'lactoseFree', 'title', 'description', 'ingredients', 'recipe'];
	public $timestamps = true;

	public function postUser() {
		return $this->hasOne('User', 'id', 'idUser');
	}

}