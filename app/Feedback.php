<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model {

	protected $table = 'feedback';
	protected $fillable = ['idPost', 'idUser', 'rating'];
	public $timestamps = true;

	public function userFeedback() {
		return $this->hasOne('User', 'id', 'idUser');
	}

	public function postFeedback() {
		return $this->hasOne('Post', 'id', 'idPost');
	}

}